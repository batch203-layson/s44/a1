const UserAccount = require("../models/UserAccount");
const ProductItem = require("../models/ProductItem");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.registerUserAccount = (req, res)=>{

    let newUser = UserAccount({

        userAccountFirstName: req.body.userAccountFirstName,
        userAccountLastName: req.body.userAccountLastName,
        userAccountEmail: req.body.userAccountEmail,
        /* 
        userAccountPassword: req.body.userAccountPassword,
         */
        userAccountPassword: bcrypt.hashSync(req.body.userAccountPassword, 10),
        userAccountMobileNumber: req.body.userAccountMobileNumber

    })

    return newUser.save()
    .then(UserAccount=>{
        res.send({message:"Registration Successful"});
    })
    .catch(error=>{
        res.send(false);
    })
}

// User Login Authentication

module.exports.loginUser = (req, res) => {
    return UserAccount.findOne({ userAccountEmail: req.body.userAccountEmail })
        .then(result => {
            // User does not exists
            if (result == null) {
                // return res.send(false);
                return res.send({ message: "User not registered yet" });
            }
            // User exists
            else {
                // Syntax: bcrypt.compareSync(data, encrypted)
                const isPasswordCorrect = bcrypt.compareSync(req.body.userAccountPassword, result.userAccountPassword);

                // If the passwords match/result of the above code is true.
                if (isPasswordCorrect) {
                    // Generate an access token
                    // Uses the "createAccessToken" method defined in the "auth.js" file
                    // Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
                    return res.send(
                        [
                            { message: "Login success" },
                            { accessToken: auth.createAccessToken(result) }
                        ]
                        );
                    
                }
                else {
                    // return false; // if password do not match
                    return res.send({ message: "Password is incorrect!" });
                }
            }
        })
}

// Route for user details

	 module.exports.getProfile = (req, res) => {
	 	//console.log(req.params.id);

         return UserAccount.findById(req.params.userAccountId).then(result =>{
            result.userAccountPassword = "***";
	 		res.send(result);
	})
	 }



module.exports.getUserDetails = (req, res) => {

    return UserAccount.findById(req.params.userAccountId).then(result => res.send(result));
}




/* 
Create Order
// Route for viewing a specific user detail
router.get("/createorder", userAccountControllers.userAccountCreateOrder);
 */


module.exports.userAccountCreateOrder=asyn(req, res)=>{

    const userData = auth.decode(req.headers.authorization);

    if (userData.userAccountIsAdmin) 
    {
      return res.send({message:"Please login using non-admin account to proceed creating an order"})
    }
    else 
    {
        let isUserUpdated = await UserAccount.findById(userData.UserAccountId)
        .then(user=>{user.requestOrders.push({
            requestOrdersTotalAmount: req.body.requestOrdersTotalAmount,
            productItems: req.body.productItems
        })})
    }


    let isUserUpdated = UserAccount.findById(userData.id).then(user => {
        

        UserAccount.userAccountOrders.push({
            requestOrdersTotalAmount: req.body.requestOrdersTotalAmount,
            products: req.body.products
        })

        // user.orders.slice(-1).pop().products.push({
        // user.orders.products.push
        //     productId: data.productId,
        //     productName: data.productName,
        //     quantity: data.quantity
        // });

        // Save the updated user information in the database
        return user.save()
            .then(result => {
                console.log(result);
                res.send(result);
            })
            .catch(error => {
                console.log(error);
                return false;
            })
    })
}